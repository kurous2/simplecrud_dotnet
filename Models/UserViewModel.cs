﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnboardDemo.Models
{
    public class UserViewModel
    {
        public string? Email { get; set; }
        public string UserName { get; set; }
        public string? FullName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthdate { get; set; }
        public string? PhoneNumber { get; set; }

    }

    public class UserLogin : UserViewModel
    {
        [Column("Username")]
        [Required]
        public string Username { get; set; }

        [Column("Password")]
        [Required]
        public string Password { get; set; }

    }

    public class UserRegister : UserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password and confirmation not match.")]
        public string PasswordConfirmation { get; set; }

    }
}
