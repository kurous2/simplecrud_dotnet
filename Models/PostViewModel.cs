﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnboardDemo.Models
{
    public class PostViewModel
    {
        public string Title { get; set; }
        public virtual string Slug { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string Content { get; set; }
        public virtual string UserName { get; set; }

    }

    public class PostList
    {
        public List<PostViewModel> PostLists { get; set; }

        public PostList()
        {
            PostLists = new List<PostViewModel>();
        }

        public void Add(PostViewModel post)
        {
            PostLists.Add(post);
        }
    }

    public class PostCreate : PostViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public override string? Slug { get; set; }

        public override string? UserName { get; set; }
    }
}
