﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Identity;
using Microsoft.AspNetCore.Identity;
using OnboardDemo.Models;

namespace OnboardDemo.Services.Contracts
{
    public interface IUserService : IServiceBase<User>
    {
        new Task<List<UserViewModel>> ToListAsync();
        Task<User> GetUserAsync(string username);
        Task<UserViewModel> GetUserVMAsync(string username);
        Task<UserRegister> GetUserRegisterVMAsync(string username);
        Task<IdentityResult> CreateAsync(UserRegister newUser);
        Task UpdateAsync(UserRegister updateUser);
        Task DeleteAsync(string username);

        Task<SignInResult> SignIn(UserLogin loginViewModel);
        Task SignIn(UserRegister registerViewModel);

        Task SignOut();

        bool Exists(string username);

    }
}
