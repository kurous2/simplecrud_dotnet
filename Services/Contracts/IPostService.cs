﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnboardDemo.Models;
using Post = DataAccess.Models.Post;

namespace OnboardDemo.Services.Contracts
{
    public interface IPostService : IServiceBase<Post>
    {
        new Task<List<PostViewModel>> ToListAsync();
        Task<PostViewModel> GetPostVMAsync(string slug);
        Task<PostCreate> GetPostCreateVMAsync(string slug);
        Task<Post> GetPostAsync(string slug);
        Task CreateAsync(PostCreate post);
        Task UpdateAsync(PostCreate model);
        Task DeleteAsync(string slug);
        bool Exists(string slug);


    }
}
