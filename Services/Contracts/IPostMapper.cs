﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using OnboardDemo.Models;

namespace OnboardDemo.Services.Contracts
{
    public interface IPostMapper
    {
        Task<PostViewModel> ModelToVM(Post post);
        Task<Post> VMToModel(PostViewModel post);
        Task<Post> CreateVMToModel(PostCreate post);
        Task<PostCreate> ModelToCreateVM(Post post);

    }
}
