﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Identity;
using OnboardDemo.Models;

namespace OnboardDemo.Services.Contracts
{
    public interface IUserMapper
    {
        Task<UserViewModel> ModelToVM(User user);
        Task<User> VMToModel(UserViewModel user);
        Task<User> RegisterVMToModel(UserRegister user);
        Task<UserRegister> ModelToRegisterVM(User user);

    }
}
