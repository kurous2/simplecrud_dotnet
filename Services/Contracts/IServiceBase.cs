﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnboardDemo.Services.Contracts
{
    public interface IServiceBase<T>
    {
        Task CreateAsync(T entity);
        Task<T> GetItemAsync(T entity);
        Task<List<T>> ToListAsync();
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        bool Exists(T entity);

    }
}
