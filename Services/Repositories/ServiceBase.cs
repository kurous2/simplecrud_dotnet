﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using OnboardDemo.Services.Contracts;

namespace OnboardDemo.Services.Repositories
{
    public class ServiceBase<T> : IServiceBase<T> where T:class
    {
        private ApplicationDbContext _db;

        public ServiceBase(ApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        public async Task CreateAsync(T entity)
        {
            _db.Set<T>().Add(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _db.Set<T>().Remove(entity);
            await _db.SaveChangesAsync();
        }

        public bool Exists(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<T> GetItemAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> ToListAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _db.Set<T>().Update(entity);
            await _db.SaveChangesAsync();
        }

    }
}
