﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;

namespace OnboardDemo.Services.Repositories
{
    public class UserService : ServiceBase<User>, IUserService
    {
        private bool isPersistent = false;
        private readonly ApplicationDbContext _db;
        private readonly IUserMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserService(ApplicationDbContext context, IUserMapper mapper, UserManager<User> userManager,
            SignInManager<User> signInManager) : base(context)
        {
            _db = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<IdentityResult> CreateAsync(UserRegister newUser)
        {
            return await _userManager.CreateAsync(await _mapper.RegisterVMToModel(newUser), newUser.Password);
        }

        public async Task DeleteAsync(string username)
        {
            var user = await GetUserAsync(username);
            await base.DeleteAsync(user);
        }

        public bool Exists(string username)
        {
            return _db.Users.Any(e => e.UserName == username);
        }

        public async Task<User> GetUserAsync(string username)
        {
            return await _db.Users.FirstAsync(m => m.UserName == username);
        }

        public async Task<UserViewModel> GetUserVMAsync(string username)
        {
            return await _mapper.ModelToVM(await GetUserAsync(username));
        }
        public async Task<UserRegister> GetUserRegisterVMAsync(string username)
        {
            return await _mapper.ModelToRegisterVM(await GetUserAsync(username));
        }

        public async Task UpdateAsync(UserRegister updateUser)
        {
            var user = await GetUserAsync(updateUser.UserName);

            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, updateUser.Password);


            user.UserName = updateUser.UserName;
            user.FullName = updateUser.FullName;
            user.Email = updateUser.Email;
            user.Birthdate = updateUser.Birthdate;
            user.PhoneNumber = updateUser.PhoneNumber;
            await base.UpdateAsync(user);
        }

        public new async Task<List<UserViewModel>> ToListAsync()
        {
            var users = await base.ToListAsync();
            List<UserViewModel> result = new();
            foreach (var user in users)
            {
                result.Add(await _mapper.ModelToVM(user));
            }
            return result;
        }

        public async Task<SignInResult> SignIn(UserLogin loginViewModel)
        {
            return await _signInManager.PasswordSignInAsync(loginViewModel.Username, loginViewModel.Password, isPersistent, false);
        }

        public async Task SignIn(UserRegister registerViewModel)
        {
            await _signInManager.SignInAsync(await _mapper.RegisterVMToModel(registerViewModel), isPersistent);
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

    }
}
