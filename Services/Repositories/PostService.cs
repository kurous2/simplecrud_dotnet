﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;
using Slugify;

namespace OnboardDemo.Services.Repositories
{
    public class PostService : ServiceBase<Post>,IPostService
    {
        private readonly ApplicationDbContext _db;
        private readonly IPostMapper _mapper;

        public PostService(ApplicationDbContext context, IPostMapper mapper) : base(context)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task CreateAsync(PostCreate post)
        {
            var result = await _mapper.CreateVMToModel(post);
            await base.CreateAsync(result);
        }

        public async Task<Post> GetPostAsync(string slug)
        {
            return await _db.Posts.FirstAsync(p => p.Slug == slug);
        }

        public async Task<PostViewModel> GetPostVMAsync(string slug)
        {
            return await _mapper.ModelToVM(await GetPostAsync(slug));
        }
        public async Task<PostCreate> GetPostCreateVMAsync(string slug)
        {
            return await _mapper.ModelToCreateVM(await GetPostAsync(slug));
        }

        public new async Task<List<PostViewModel>> ToListAsync()
        {
            var posts = await base.ToListAsync();
            List<PostViewModel> postviews = new();
            foreach (var post in posts)
            {
                postviews.Add(await _mapper.ModelToVM(post));
            }
            return postviews;
        }

        public async Task UpdateAsync(PostCreate model)
        {
            SlugHelper slugHelper = new SlugHelper();

            var post = await GetPostAsync(model.Slug);
            post.UpdatedDate = DateTime.Now;
            post.Title = model.Title;
            post.Slug = slugHelper.GenerateSlug(model.Title);
            post.Content = model.Content;
            post.UserId = (await _db.Users.FirstAsync(u => u.UserName == model.UserName)).Id;
            await base.UpdateAsync(post);
        }

        public async Task DeleteAsync(string slug)
        {
            var target = await GetPostAsync(slug);
            await base.DeleteAsync(target);
        }

        public bool Exists(string slug)
        {
            return _db.Posts.Any(e => e.Slug == slug);
        }

    }
}
