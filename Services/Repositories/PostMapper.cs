﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;
using Slugify;


namespace OnboardDemo.Services.Repositories
{
    public class PostMapper : IPostMapper
    {
        private ApplicationDbContext _dbContext;

        public PostMapper(ApplicationDbContext context)
        {
            _dbContext = context;
        }
        public async Task<PostViewModel> ModelToVM(Post post)
        {
            PostViewModel model = new PostViewModel
            {
                Title = post.Title,
                Slug = post.Slug,
                Content = post.Content,
                CreatedDate = post.CreatedDate,
            };

            // get models username from database
            model.UserName = (await _dbContext.Users.FirstAsync(u => u.Id == post.UserId)).UserName;
            return model;
        }

        public async Task<Post> VMToModel(PostViewModel post)
        {
            SlugHelper helper = new SlugHelper();
            Post model = new()
            {
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                CreatedDate = post.CreatedDate,
            };

            // get models UserId from database
            model.UserId = (await _dbContext.Users.FirstAsync(u => u.UserName == post.UserName)).Id;
            return model;
        }

        public async Task<Post> CreateVMToModel(PostCreate post)
        {
            SlugHelper helper = new SlugHelper();
            Post model = new()
            {
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                UserId = (await _dbContext.Users.FirstAsync(u => u.UserName == post.UserName)).Id
            };
            return model;
        }
        public async Task<PostCreate> ModelToCreateVM(Post post)
        {
            SlugHelper helper = new SlugHelper();
            PostCreate model = new()
            {
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                UserName = (await _dbContext.Users.FirstAsync(u => u.Id == post.UserId)).UserName
            };
            return model;
        }

    }
}
