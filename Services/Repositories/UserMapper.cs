﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Identity;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;

namespace OnboardDemo.Services.Repositories
{
    public class UserMapper : IUserMapper
    {
        public Task<UserViewModel> ModelToVM(User user)
        {
            UserViewModel userView = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            return Task.FromResult(userView);
        }

        public Task<User> RegisterVMToModel(UserRegister user)
        {
            User newUser = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };
            return Task.FromResult(newUser);
        }

        public Task<User> VMToModel(UserViewModel user)
        {
            throw new NotImplementedException();
        }
        public Task<UserRegister> ModelToRegisterVM(User user)
        {
            UserRegister newUser = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };
            return Task.FromResult(newUser);
        }

    }
}
