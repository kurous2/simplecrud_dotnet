﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;

namespace OnboardDemo.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private readonly IPostService _service;
        private IPostMapper _mapper;

        public PostController(IPostService postService, IPostMapper mapper)
        {
            _service = postService;
            _mapper = mapper;
        }

        // GET: Post
        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await _service.ToListAsync());

            //return View(await _mapper.Posts.ToListAsync());
        }

        // GET: Post/Details/5
        [Authorize]
        public async Task<IActionResult> Details(string? slug)
        {

            if (slug == null)
            {
                return NotFound();
            }

            var post = await _service.GetPostVMAsync(slug);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Post/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Post/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PostCreate post)
        {
            var username = User.Identity.Name;
            post.UserName = username;

            if (ModelState.IsValid)
            {
                await _service.CreateAsync(post);
                return RedirectToAction(nameof(Index));
            }

            return View(post);
        }

        // GET: Post/Edit/slug
        [Authorize]
        public async Task<IActionResult> Edit(string? slug)
        {
            if (slug == null)
            {
                return NotFound();
            }

            var post = await _service.GetPostCreateVMAsync(slug);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: Post/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string slug, PostCreate post)
        {
            if (slug != post.Slug)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    post.UserName = User.Identity.Name;
                    await _service.UpdateAsync(post);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_service.Exists(slug))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Post/Delete/slug
        [Authorize]
        public async Task<IActionResult> Delete(string slug)
        {
            if (slug == null)
            {
                return NotFound();
            }

            var post = await _service.GetPostVMAsync(slug);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Post/Delete/slug
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string slug)
        {
            await _service.DeleteAsync(slug);
            return RedirectToAction(nameof(Index));
        }
    }

}

