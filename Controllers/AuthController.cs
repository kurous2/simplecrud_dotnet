﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnboardDemo.Models;
using OnboardDemo.Services.Contracts;

namespace OnboardDemo.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService _service;
        private readonly IUserMapper _mapper;

        public AuthController(IUserService userService, IUserMapper mapper)
        {
            _service = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLogin loginResponse)
        {

            if (ModelState.IsValid)
            {

                var result = await _service.SignIn(loginResponse);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Post");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }

            // Authentication fails
            return View(loginResponse);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _service.SignOut();

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(UserRegister registerResponse)
        {
            Dictionary<string, string> messages = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                var result = await _service.CreateAsync(registerResponse);

                if (result.Succeeded)
                {
                    await _service.SignIn(registerResponse);
                    return RedirectToAction("Index", "Post");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                foreach (KeyValuePair<string, string> entry in messages)
                {
                    ModelState.AddModelError(entry.Key, entry.Value);
                }

                ModelState.AddModelError(string.Empty, "Invalid Registration Attempt");
            }

            // Registration fails
            return View(registerResponse);
        }

    }
}
